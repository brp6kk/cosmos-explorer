let user_imgs = [
    "img/final/rocket1.png",
    "img/final/rocket2.png",
    "img/final/rocket3.png",
    "img/final/rocket4.png"
];

let object_imgs = [
    [
        "img/final/galaxy1.png", "img/final/galaxy2.png", "img/final/galaxy3.png", 
        "img/final/galaxy4.png", "img/final/galaxy5.png"
    ],
    [
        "img/final/meteor1.png", "img/final/meteor2.png", "img/final/meteor3.png", 
        "img/final/meteor4.png"
    ],
    [
        "img/final/planet1-1.png", "img/final/planet1-2.png", "img/final/planet1-3.png", 
        "img/final/planet1-4.png"
    ],
    ["img/final/planet2-1.png", "img/final/planet2-2.png", "img/final/planet2-3.png"],
    [
        "img/final/planet3-1.png", "img/final/planet3-2.png", "img/final/planet3-3.png", 
        "img/final/planet3-4.png"
    ],
    ["img/final/satellite.png"],
    [   "img/final/shootingstar1.png", "img/final/shootingstar2.png", "img/final/shootingstar3.png", 
        "img/final/shootingstar4.png"
    ],
    [
        "img/final/sun1.png", "img/final/sun2.png", "img/final/sun3.png", 
        "img/final/sun4.png"],
    ["img/final/ufo1.png", "img/final/ufo2.png", "img/final/ufo1.png"]
];

let user;
let objects = [];

let num_rows = 9;
let num_cols = 12;

let easing = 0.025;

/* Load in images and set objects up in space. */
function preload() {
    // Load in random rocket ship
    let img_index = Math.floor(Math.random() * user_imgs.length);
    let user_image = loadImage(user_imgs[img_index]);
    user = {
        "image": user_image,
        "x": innerWidth / 2 - user_image.width / 2,
        "y": innerHeight / 2 - user_image.height / 2,
        "angle": 0
    }

    // Populate space with random objects
    for (let col = 0; col < num_cols; col++) {
        for (let cell = 0; cell < num_rows; cell++) {
            // Grab random object of random color
            let obj_index = Math.floor(Math.random() * 9);
            let color_index = Math.floor(Math.random() * object_imgs[obj_index].length)
            let img = loadImage(object_imgs[obj_index][color_index]);

            // Place object in space such that it isn't too close to other objects
            let x = Math.random() * innerWidth + innerWidth * col;
            let y = Math.random() * innerHeight + innerHeight * cell;
            let angle = Math.random() * Math.PI * 2;

            obj = {
                "image": img,
                "x": x,
                "y": y,
                "angle": angle,
            }

            // Galaxies should be drawn before other objects
            if (obj_index == 0) {
                objects.splice(0, 0, obj);
            } else {
                objects.push(obj);
            }
            
        }
    }
}

/* Basic setup after loading in images */
function setup() {
    createCanvas(innerWidth, innerHeight);

    imageMode(CENTER);
}

/* Update the location of objects in space as the rocket moves */
function update_images() {
    scenery_update();
    rocket_update();
}

/* Update the location of background scenery */
function scenery_update() {
    // Move horizontally if user is on left/rightmost area of screen
    let dx = 0;
    if (user["x"] < innerWidth / 4) {
        dx += 5;
    } else if (user["x"] > innerWidth * 3 / 4){
        dx -= 5;
    }

    // Move vertically if user is on top/bottommost area of screen
    let dy = 0;
    if (user["y"] < innerHeight / 4) {
        dy += 5;
    } else if (user["y"] > innerHeight * 3 / 4) {
        dy -= 5;
    }

    // Move all objects appropriately
    for (let i = 0; i < objects.length; i++) {
        objects[i]["x"] += dx;
        // Modulus on a negative number acts strange,
        // so move the object to the rightmost edge of the map
        if (objects[i]["x"] < 0) {
            objects[i]["x"] += num_cols * (innerWidth +objects[i]["image"].width);
        }
    
        // Modulus on a negative number acts strange,
        // so move the object to the bottommost edge of the map
        objects[i]["y"] += dy;
        if (objects[i]["y"]  < 0) {
            objects[i]["y"] += num_rows*(innerHeight +objects[i]["image"].height);
        }
    

        // Calculate coordinate for object.
        // Use modulus so that we can achieve endless scrolling
        // by repeatedly viewing objects
        let x = (objects[i]["x"] % (num_cols * (innerWidth + objects[i]["image"].width))) 
                - objects[i]["image"].width;
        let y = (objects[i]["y"] % (num_rows * (innerHeight + objects[i]["image"].height))) 
                - objects[i]["image"].height;

        // Rotate and place the object
        push();
        translate(x, y);
        rotate(objects[i]["angle"])
        image(objects[i]["image"], 0, 0)
        pop();
    }
}

/* Update the location and angle of the rocket */
function rocket_update() {
    // Gradually move rocket to current mouse location
    let targetX = mouseX;
    let dx = targetX - user["x"];
    user["x"] += dx * easing;

    let targetY = mouseY;
    let dy = targetY - user["y"];
    user["y"] += dy * easing;

    // Rotate user's rocket.
    // Correction is needed since Math.atan2 returns a value
    // between -pi and pi. Without correction, some angle changes
    // appear to jump.
    let targetAngle = Math.atan2(dy, dx);
    let correction = 0;
    if (targetAngle < 0 && user["angle"] - targetAngle > Math.PI) {
        correction = 2*Math.PI;
    } else if (targetAngle > 0 && targetAngle - user["angle"] > Math.PI) {
        correction = -2*Math.PI;
    }
    targetAngle += correction;
    let da = targetAngle - user["angle"];
    user["angle"] += da * easing * 4;
    if (user["angle"] > Math.PI || user["angle"] < -Math.PI) {
        user["angle"] -= correction;
    }

    // Rotate & place the rocket.
    push();
    translate(user["x"], user["y"]);
    rotate(user["angle"] + radians(90));
    image(user["image"], 0, 0);
    pop();
}

/* Draw the scene */
function draw() {
    background(0, 0, 50);

    update_images();
}