# Cosmos Explorer

Uncommon Hacks 2022 Project - Eric Y Chang

## Description

* Move your cursor to explore the galaxy
* Programmatic art project built using [p5.js](https://p5js.org/)
* All art created by me

## Ready to play?

### General Instructions

* Move your cursor to explore the galaxy
* No goals - just have fun exploring!
* Note - if you have trouble finding objects, it may be that the randomization placed them poorly. Try refreshing the page!

### Online

Visit [this link](https://brp6kk.gitlab.io/cosmos-explorer/)

### Locally

* Download the repository
* Launch a webserver in the `src` directory
  * Recommendation: [http-server](https://www.npmjs.com/package/http-server)
* Navigate to any url on which the server is available
